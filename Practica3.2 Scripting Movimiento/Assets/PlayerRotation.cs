﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{

    [SerializeField]
    public Vector3 RotationAxis;
    public float RotateSpeed;


    // Update is called once per frame
    void Update()
    {
        RotationAxis = PlayerMovement.ClampVector3(RotationAxis);

        transform.Rotate(RotationAxis * (RotateSpeed * Time.deltaTime));
    }
}



