﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFPSMovement : MonoBehaviour
{
    public GameObject PlayerCam;
    public float WalkSpeed;
    public float hRotationSpeed;
    public float vRotationSpeed;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        //Puesto que mi personaje tiene físicas no lo desactivo para que pueda interactuar con el entorno
        //GameObject.Find("Player").gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Movement();  
    }

    void Movement()
    {
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");

        Vector3 moveDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        transform.Translate(moveDirection * (WalkSpeed * Time.deltaTime));

        float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
        float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;

        transform.Rotate(0f, hPlayerRotation, 0f);
        PlayerCam.transform.Rotate(-vCamRotation, 0f, 0f);
    }
}
