﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScale : MonoBehaviour
{
    [SerializeField]
    public Vector3 ScaleAxis;
    public float ScalingValue;


    // Update is called once per frame
    void Update()
    {
        ScaleAxis = PlayerMovement.ClampVector3(ScaleAxis);

        transform.localScale += ScaleAxis * (ScalingValue * Time.deltaTime);
    }
}

