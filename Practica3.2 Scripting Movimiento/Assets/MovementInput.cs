﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementInput : MonoBehaviour
{
    public float MoveSpeed;
    public float AngularSpeed;

    // Update is called once per frame
    void Update()
    {        
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.up * (-AngularSpeed * Time.deltaTime));
        }

        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.up * (AngularSpeed * Time.deltaTime));
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.forward * (MoveSpeed * Time.deltaTime));
        }

        else if(Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.back * (MoveSpeed * Time.deltaTime));
        }

        if (Input.GetKey(KeyCode.Space))
        {
            transform.Translate(Vector3.up * (MoveSpeed * Time.deltaTime));
        }

        if(transform.position.y <= -620)
        {
            transform.position = new Vector3(0,500,3);
           
        }

    }
}
