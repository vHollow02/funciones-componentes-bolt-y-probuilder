﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public GameObject PlayerCamera;
    public float hRotationSpeed;
    public float vRotationSpeed;
    public float maxVertical;
    public float minVertical;
    public float smoothTime;

    float vCamRot;
    float hPlayerRot;
    float hVelocity;
    float vVelocity;
    float targetCamEulers;
    Vector3 targetCamRotation;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    public void handleRotation(float hInput, float vInput)
    {
        //Get Rotation
        float targetPlayerRotation = hInput * hRotationSpeed * Time.deltaTime;
        targetCamEulers += vInput * vRotationSpeed * Time.deltaTime;

        //Set Player Rotation
        hPlayerRot = Mathf.SmoothDamp(hPlayerRot, targetPlayerRotation, ref hVelocity, smoothTime);
        transform.Rotate(0f, hPlayerRot, 0f);

        //Set Cam Rotation
        targetCamEulers = Mathf.Clamp(targetCamEulers, minVertical, maxVertical);
        vCamRot = Mathf.SmoothDamp(vCamRot, targetCamEulers, ref vVelocity, smoothTime);
        targetCamRotation.Set(-vCamRot, 0f, 0f);
        PlayerCamera.transform.localEulerAngles = targetCamRotation;
    }
}
