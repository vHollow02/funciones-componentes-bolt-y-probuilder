﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{

    public float speed = 2f;
    public float jumpHeight = 2f;
    [Range(0.01f, 1f)]
    public float forwardJumpFactor = 0.05f;
    public float Gravity;
    public float DashValue = 2f;
    public Vector3 Drag = new Vector3(1f, 2f, 1f);
    public float smoothTime = 0.15f;

    private CharacterController characterControl;
    public Vector3 moveDir;
    private Vector3 smoothDir;
    private Vector3 smoother;
    private Vector3 horizontalSpeed;

    private bool isGrounded { get { return characterControl.isGrounded; } }
    private float currentSpeed { get { return horizontalSpeed.magnitude; } }

    private float currentNormalizedSpeed { get { return horizontalSpeed.normalized.magnitude; } }

    // Start is called before the first frame update
    void Start()
    {
        characterControl = GetComponent<CharacterController>();
    }

   
    // Update is called once per frame
    public void moveCharacter(float hInput, float vInput, bool jump, bool dash)
    {        
        float deltaTime = Time.deltaTime;
        float dashF = 1f;

        if(characterControl.isGrounded)
        {
            moveDir += (hInput * transform.right + vInput * transform.forward).normalized;

            if (dash) dashF = DashValue;
            if (jump)
            {
                if (Mathf.Abs(moveDir.x) > 0f || Mathf.Abs(moveDir.z) > 0f)
                {
                    moveDir += moveDir.normalized * (Mathf.Sqrt(jumpHeight * forwardJumpFactor * -Gravity / 2) * dashF);
                }
                moveDir.y = Mathf.Sqrt(jumpHeight * -2f * Gravity);
            }
        }

        moveDir.y += Gravity * deltaTime;

        moveDir.x /= 1 + Drag.x * deltaTime;
        moveDir.y /= 1 + Drag.y * deltaTime;
        moveDir.z /= 1 + Drag.z * deltaTime;

        smoothDir = Vector3.SmoothDamp(smoothDir, moveDir, ref smoother, smoothTime);
               
        smoothDir.y = moveDir.y;

        characterControl.Move(smoothDir * (deltaTime * speed * dashF));

        horizontalSpeed.Set(characterControl.velocity.x, 0, characterControl.velocity.z);

    }


}
